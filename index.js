// Load the expressjs module into our application and saved it a variable called express.

// Syntax: require("package");
const express = require("express");

// localhost port number
const port = 4000;

// app is our server

// create an application that uses express and stores it as app.
const app = express();
// Middleware
//  express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body.

app.use(express.json());

// mock data
let users = [
  {
    username: "TStark3000",
    email: "starkindustries@mail.com",
    password: "notPeterParker",
  },
  {
    username: "ThorThunder",
    email: "loveAndThunder@mail.com",
    password: "iLoveStormBreaker",
  },
];
let items = [
  {
    name: "Mjolnir",
    price: 50000,
    isActive: true,
  },
  {
    name: "Vibranium Shield",
    price: 70000,
    isActive: true,
  },
];
// Express has methods to use as routes corresponding to HTTP methods
// Syntax: app.method(<endpoint>, <function for request and response>)

app.get("/", (req, res) => {
  // res.send() actually combines writeHead() and end()
  res.send("Hello from my first expressJS API");
  // res.status(200).send("message")
});

app.get("/greeting", (req, res) => {
  res.send("Hello from Batch203-Peñaflorida");
});

// retrieval of the users in mock database
app.get("/users", (req, res) => {
  res.send(users);
});

// POST
// adding of new user
app.post("/users", (req, res) => {
  console.log(req.body); //result: {} empty
  // simulate creating a new user document
  let newUser = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
  };
  users.push(newUser);
  console.log(users);
  res.send(users);
});

// PUT
// Update Users password
// :index - wildcard
// url: localhost:4000/users/5
app.put("/users/:index", (req, res) => {
  console.log(req.body); //update password
  console.log(req.params); //refer to the wildcard in the endpoint
  // parseInt the value of the number coming from req.params
  // ['0']
  let index = parseInt(req.params.index);
  console.log(index);
  // users[0].password
  // password = password
  // the update will be coming from the request body.
  // This will be done using reassignment.
  users[index].password = req.body.password;
  // send the updated user
  res.send(users[index]);
});

// DELETE
app.delete("/users", (req, res) => {
  users.pop();
  res.send(users);
});
// Get Item

app.get("/items", (req, res) => {
  res.send(items);
});
app.post("/items", (req, res) => {
  let newItem = {
    name: req.body.name,
    price: req.body.price,
    isActive: req.body.isActive,
  };
  items.push(newItem);
  res.send(items);
});
app.put("/items/:index", (req, res) => {
  let index = parseInt(req.params.index);

  items[index].price = req.body.price;

  res.send(items[index]);
});

// port listener
app.listen(port, () => console.log(`Server is running at ${port}.`));
